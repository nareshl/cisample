/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ace2three.cisample;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 *
 * @author nareshlunavath
 */
@SpringBootApplication
public class SampleApplication {
    
    public static void main(String[] args) {
        SpringApplication.run(SampleApplication.class, args);
    }
}
